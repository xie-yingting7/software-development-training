from PyQt5.QtWidgets import QDialog, QVBoxLayout, QLabel, QLineEdit, QPushButton, QMessageBox  
from PyQt5.QtCore import pyqtSlot  
import sqlite3
  
class LoginDialog(QDialog):  
    def __init__(self, parent=None):  
        super(LoginDialog, self).__init__(parent)  
        self.initUI()  
  
    def initUI(self):  
        self.setWindowTitle('登录')  
        layout = QVBoxLayout()  
  
        self.label_username = QLabel('用户名:')  
        self.line_username = QLineEdit()  
        layout.addWidget(self.label_username)  
        layout.addWidget(self.line_username)  
  
        self.label_password = QLabel('密码:')  
        self.line_password = QLineEdit()  
        self.line_password.setEchoMode(QLineEdit.Password)  
        layout.addWidget(self.label_password)  
        layout.addWidget(self.line_password)  
  
        self.btn_login = QPushButton('登录')  
        self.btn_login.clicked.connect(self.on_login)  
        layout.addWidget(self.btn_login)  
        
        self.btn_register = QPushButton('注册')  
        self.btn_register.clicked.connect(self.on_register)  
        layout.addWidget(self.btn_register)   
         
        self.btn_cancel = QPushButton('取消')  
        self.btn_cancel.clicked.connect(self.reject)  
        layout.addWidget(self.btn_cancel)  

        self.setLayout(layout)  
        
        
    @pyqtSlot()  
    def on_register(self):  
        username = self.line_username.text()  
        password = self.line_password.text()  
        if self.register_user(username, password):  
            QMessageBox.information(self, '成功', '注册成功！')  
        else:  
            QMessageBox.warning(self, '错误', '注册失败！')  
  
    def register_user(self, username, password):  
        # 连接到SQLite数据库（如果不存在则创建）  
        conn = sqlite3.connect('users.db')  
        cursor = conn.cursor()  
  
        # 创建表（如果表不存在）  
        cursor.execute('''CREATE TABLE IF NOT EXISTS users  
                          (username TEXT PRIMARY KEY, password TEXT)''')  
  
        # 插入新用户  
        try:  
            cursor.execute("INSERT INTO users (username, password) VALUES (?, ?)", (username, password))  
            conn.commit()  
            # 查询并打印表的内容  
            cursor.execute("SELECT * FROM users")  
            rows = cursor.fetchall()  # 获取所有查询结果  
            for row in rows:  
                print(row)  
            return True  
        except sqlite3.IntegrityError:  
            # 如果用户名已存在，则不插入  
            QMessageBox.warning(self, '错误', '用户名已存在！')  
            return False  
        finally:  
            conn.close()  
  
    @pyqtSlot()  
    def on_login(self):  
        username = self.line_username.text()  
        password = self.line_password.text()  
        if self.validate_credentials(username, password):  
            self.accept()  
        else:  
            QMessageBox.warning(self, '错误', '用户名或密码错误！')  
  
    def validate_credentials(self, username, password):  
        conn = sqlite3.connect('users.db')  
        cursor = conn.cursor()  
  
        # 查询数据库中的用户  
        cursor.execute("SELECT * FROM users WHERE username = ? AND password = ?", (username, password))  
        user = cursor.fetchone()  
  
        # 关闭数据库连接  
        conn.close()  
  
        # 如果找到了用户，返回 True；否则返回 False  
        return user is not None  