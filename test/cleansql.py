import sqlite3  
  
# 连接到SQLite数据库  
conn = sqlite3.connect('users.db')  
cursor = conn.cursor()  
  
try:  
    # 清空window_info表中的所有记录  
    cursor.execute("DELETE FROM users")  
    print("表已清空")  
      
    # 提交事务  
    conn.commit()  
  
except sqlite3.Error as e:  
    print(f"数据库操作出错: {e}")  
  
finally:  
    # 关闭数据库连接  
    conn.close()